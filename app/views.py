from urllib import parse

import stripe
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.views import View
from django.views.generic import DetailView, TemplateView, FormView, UpdateView, ListView

from app.forms import SignUpForm, UpdateUserPersonalInfoForm, CheckoutForm, PaymentForm
from app.models import Product, ProductCategory, Cart, CartItem, Messages, Room
from app.utils import get_session_key, cart_data, stripe_card

stripe.api_key = settings.STRIPE_SECRET_KEY

"""BLOCK: USER"""


class SignUpView(FormView):
    template_name = "signup.html"
    form_class = SignUpForm
    success_url = "/login/"

    def form_valid(self, form):
        form.save()
        return super(SignUpView, self).form_valid(form)


class UserPageView(DetailView):
    template_name = "user-profile-page.html"

    def get_object(self, queryset=None):
        if self.request.user.default_card:
            # stripe_card - function from utils.py
            return stripe_card(self.request)[0]


class UpdateUserPersonalInfoView(UpdateView):
    template_name = "update-info.html"
    model = get_user_model()
    form_class = UpdateUserPersonalInfoForm

    def get_success_url(self):
        return f'/{self.request.user.slug}/profile/'


class UserCardView(TemplateView):
    template_name = 'user-card.html'

    def get_context_data(self, **kwargs):
        context = super(UserCardView, self).get_context_data(**kwargs)
        context['STRIPE_PUBLIC_KEY'] = settings.STRIPE_PUBLIC_KEY
        try:
            context.update({'card_list': stripe_card(self.request)})
            return context
        except TypeError:
            return context

    def post(self, request):
        user = request.user
        token = request.POST['stripeToken']
        if not user.stripe_customer_id:
            customer = stripe.Customer.create(
                email=user.email,
                name=f'{user.first_name} {user.last_name}',
                source=token,
            )
            user.stripe_customer_id = customer['id']
            user.default_card = True
            user.save()
        else:
            if stripe_card(request):
                stripe.Customer.delete_source(
                    user.stripe_customer_id,
                    stripe_card(request)[0]['id']
                )
            stripe.Customer.create_source(
                user.stripe_customer_id,
                source=token,
            )
            user.save()

        return redirect(f'/{request.user.slug}/profile/')


class OrderHistoryView(ListView):
    template_name = 'order-history.html'

    def get_queryset(self):
        return Cart.objects.filter(session_key=get_session_key(self.request), ordered=True).all().prefetch_related(
            'items')


"""ENDBLOCK: USER"""


class MainView(TemplateView):
    template_name = "main.html"


"""BLOCK: Product"""


class CategoryView(TemplateView):
    template_name = "all-category-page.html"

    def get_context_data(self, **kwargs):
        context = super(CategoryView, self).get_context_data(**kwargs)
        context["items_rating"] = Product.objects.all().order_by('-rating')[0:4]
        return context


class ProductView(ListView):
    template_name = "selected-category.html"

    def get_queryset(self):
        category = ProductCategory.objects.filter(slug=self.kwargs['slug']).first()
        return Product.objects.filter(category=category)


class ProductInfoView(DetailView):
    model = Product
    template_name = "detail-product-info.html"


"""ENDBLOCK: Product"""

"""BLOCK: CART"""


class AddToCartView(View):

    def post(self, request):
        item = get_object_or_404(Product, id=request.POST['item_id'])
        order_item, created = CartItem.objects.get_or_create(
            item=item,
            ordered=False,
        )
        if cart_data(request).exists():
            cart = cart_data(request).first()
            if cart.items.filter(item__title=item.title).exists():
                order_item.quantity += 1
                order_item.save()
            else:
                cart.items.add(order_item)
        else:
            cart = Cart.objects.create(session_key=get_session_key(request),
                                       user=request.user if request.user.is_authenticated else None)
            cart.items.add(order_item)
        return JsonResponse({'status': 'Added to cart'})


class DeleteElementFromCartView(View):

    def post(self, request):
        item = get_object_or_404(CartItem, id=request.POST['del_item'])
        item_on_delete = CartItem.objects.filter(id=item.id)
        item_on_delete.delete()
        if not cart_data(request).first().items.exists():
            cart_data(request).delete()
            return JsonResponse({})
        return JsonResponse({})


class DeleteAllElementsFromCartView(View):

    def post(self, request):
        cart = get_object_or_404(Cart, id=request.POST['cart_id'])
        cart.items.all().delete()
        cart.delete()
        return JsonResponse({})


class CartView(TemplateView):
    template_name = "cart.html"


"""ENDBLOCK: CART"""

"""BLOCK: CHECKOUT"""

"""TEST"""

# class CheckoutView(UpdateView):
#     template_name = 'checkout.html'
#     model = Cart
#     form_class = CheckoutForm
#     success_url = 'success'
#     pk_url_kwarg = 'id'
# 
#     def get_context_data(self, **kwargs):
#         context = {
#             'form': CheckoutForm(
#                 initial={
#                     'first_name': self.request.user.first_name if self.request.user.is_authenticated else None,
#                     'last_name': self.request.user.last_name if self.request.user.is_authenticated else None,
#                     'phone_number': self.request.user.phone_number if self.request.user.is_authenticated else None,
#                     'address': self.request.user.address if self.request.user.is_authenticated else None,
#                 }
#             ),
#         }
#         if self.request.user.is_authenticated:
#             context['bonus_c'] = True if self.request.user.is_authenticated and self.request.user.bonus_coins \
#                                          >= cart_data(self.request).first().get_total() else False,
#         return context
# 
#     def get_form_kwargs(self):
#         form_kwargs = super(CheckoutView, self).get_form_kwargs()
#         form_kwargs = {
#             'request': self.request,
#             "user": self.request.user,
#             "cart_data": cart_data(self.request),
#         }
#         return form_kwargs
# 
#     def form_valid(self, form):
#         if not form.is_valid():
#             form.save()
#             return redirect('success')
#         else:
#             messages.warning(self.request, "Something went wrong")
#             return redirect("/")
#         return super(CheckoutView, self).form_valid(form)
# 
#     # def post(self, request):


"""TEST"""


class CheckoutView(TemplateView):
    template_name = 'checkout.html'

    def get_context_data(self, **kwargs):
        context = super(CheckoutView, self).get_context_data(**kwargs)
        context = {
            'form': CheckoutForm(
                initial={
                    'first_name': self.request.user.first_name if self.request.user.is_authenticated else None,
                    'last_name': self.request.user.last_name if self.request.user.is_authenticated else None,
                    'phone_number': self.request.user.phone_number if self.request.user.is_authenticated else None,
                    'address': self.request.user.address if self.request.user.is_authenticated else None,
                }
            ),
        }
        if self.request.user.is_authenticated:
            context['bonus_c'] = True if self.request.user.is_authenticated and self.request.user.bonus_coins \
                                         >= cart_data(self.request).first().get_total() else False,
        return context

    def post(self, request):
        if request.method == 'POST':
            form = CheckoutForm(request.POST)
            if form.is_valid():
                cart = Cart.objects.filter(session_key=get_session_key(request), ordered=False).first()
                cart.first_name = form.cleaned_data.get('first_name')
                cart.last_name = form.cleaned_data.get('last_name')
                cart.phone_number = form.cleaned_data.get('phone_number')
                cart.address = form.cleaned_data.get('address')
                cart.description = form.cleaned_data.get('description')
                cart.payment_choice = form.cleaned_data.get('payment_choice')
                cart.save()
                if form.cleaned_data.get('payment_choice') == 'Bonus coins':
                    request.user.bonus_coins -= cart.get_total()
                    request.user.save()
                elif form.cleaned_data.get('payment_choice') == 'Cash':
                    if request.user.is_authenticated:
                        request.user.bonus_coins += int(cart_data(self.request).first().get_total()) * 0.01
                        request.user.save()
                elif form.cleaned_data.get('payment_choice') == 'Card':
                    return redirect('create-charge')
                for cartitem in cart.items.all():
                    cartitem.ordered = True
                    cartitem.item.rating += 1
                    cartitem.item.save()
                    cartitem.save()
                cart.ordered = True
                cart.save()
                return redirect('success')
            else:
                messages.warning(self.request, "Something went wrong")
                return redirect("/")


class ChargeView(TemplateView):
    template_name = 'create-charge.html'

    def get_context_data(self, **kwargs):
        user = self.request.user
        context = {
            'cart': cart_data(self.request).first(),
            'form': CheckoutForm,
            'STRIPE_PUBLIC_KEY': settings.STRIPE_PUBLIC_KEY,
        }
        if user.is_authenticated:
            if user.default_card:
                card_list = stripe_card(self.request)
                if len(card_list) > 0:
                    context.update({
                        'card': card_list[0]
                    })
                return context
            return context
        return context

    def post(self, request):
        user = request.user
        cart = cart_data(self.request).first()
        form = PaymentForm(self.request.POST)
        if form.is_valid():
            token = form.cleaned_data.get('stripeToken')
            use_default = form.cleaned_data.get('use_default')
            try:
                if use_default:
                    stripe.Charge.create(
                        amount=int(cart.get_total()) * 100,
                        currency="uah",
                        customer=user.stripe_customer_id,
                        description=f'Order #{cart.id}',
                    )
                else:

                    # # test payment
                    # stripe.Charge.create(
                    #     amount=500,
                    #     currency="usd",
                    #     source='tok_visa_chargeDeclined',
                    # )

                    stripe.Charge.create(
                        amount=int(cart.get_total()) * 100,
                        currency="uah",
                        source=token,
                        description=f'Order #{cart.id}',
                    )
                if user.is_authenticated:
                    user.bonus_coins += int(cart_data(self.request).first().get_total()) * 0.01
                    user.save()
                cart.ordered = True
                cart.save()
                for cartitem in cart.items.all():
                    cartitem.ordered = True
                    cartitem.item.rating += 1
                    cartitem.item.save()
                    cartitem.save()
                return redirect('/success/')

            except stripe.error.CardError as e:
                body = e.json_body
                err = body.get('error', {})
                messages.error(self.request, f"{err.get('message')}")
                return redirect("/")

            except stripe.error.RateLimitError:
                # Too many requests made to the API too quickly
                messages.error(self.request, "Rate limit error")
                return redirect("/")

            except stripe.error.InvalidRequestError as e:
                # Invalid parameters were supplied to Stripe's API
                print(e)
                messages.error(self.request, "Invalid parameters")
                return redirect("/")

            except stripe.error.AuthenticationError:
                # Authentication with Stripe's API failed
                messages.error(self.request, "Not authenticated")
                return redirect("/")

            except stripe.error.APIConnectionError:
                # Network communication with Stripe failed
                messages.error(self.request, "Network error")
                return redirect("/")

            except stripe.error.StripeError:
                messages.error(self.request, "Something went wrong. You were not charged. Please try again.")
                return redirect("/")


class SuccessView(TemplateView):
    template_name = 'success.html'

    def get_context_data(self, **kwargs):
        context = super(SuccessView, self).get_context_data(**kwargs)
        context['new_items'] = Product.objects.all().order_by('-added')[0:5]
        return context


class AboutUsView(TemplateView):
    template_name = 'about-us.html'


"""ENDBLOCK: CHECKOUT"""


class RoomListView(ListView):
    template_name = 'room-list.html'

    def get_queryset(self):
        return Room.objects.all()


class TechSupportView(TemplateView):
    template_name = 'tech_support.html'

    def get_context_data(self, **kwargs):
        context = super(TechSupportView, self).get_context_data(**kwargs)
        context['room'] = Room.objects.get(id=self.kwargs['room_name_id'])
        context['message'] = Messages.objects.filter(room_id=self.kwargs['room_name_id'])[:100]
        return context
