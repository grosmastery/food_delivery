from app.models import Cart
import stripe


def get_session_key(request):
    if not request.session.session_key:
        request.session.save()
    return request.user.get_session_auth_hash() if request.user.is_authenticated else request.session.session_key


def cart_data(request):
    return Cart.objects.filter(session_key=get_session_key(request), ordered=False)


def stripe_card(request):
    cards = stripe.Customer.list_sources(
        request.user.stripe_customer_id,
        object='card'
    )
    return cards['data']
