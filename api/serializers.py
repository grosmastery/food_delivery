from django.contrib.auth import get_user_model
from rest_framework import serializers

from app.models import ProductCategory, Product


class ProductCategorySerializer(serializers.Serializer):
    category = serializers.CharField()

    def save(self, **kwargs):
        return ProductCategory.objects.create(**self.validated_data)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        exclude = (
            'avatar', 'slug', 'order_history', 'groups', 'user_permissions', 'date_joined', 'last_login', 'password',
            'is_active')


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        exclude = ('added',)


class CreateProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        exclude = ('added',)
